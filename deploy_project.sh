#!/bin/bash

# Создаем папку fold1 и для проекта
mkdir fold

# Копируем файлы в папку fold
cp code/store.js fold/
cp code/package.json fold/
cp code/Document.pdf fold/
cp code/nft.png fold/

# Переходим в папку fold, устанавливаем зависимости и запускаем store.js
cd fold
touch .env
npm install
node store.js
cd ..

# Копируем stud.sol, deploy.js и getDate.js
cd hardhat
mkdir scripts
npm install dotenv
cp ../code/stud.sol contracts/
cp ../code/deploy.js scripts/
cp ../code/getDate.js scripts/

# Объединяем файлы .env
merge() {
cat $1 > tmp0
echo "" >> tmp0
cat $2 >> tmp0
awk -F "=" '!a[$1]++' tmp0 > tmp1 && mv tmp1 .env && rm tmp0
}

merge ../fold/.env ../code/date.env
cat .env

# Запускаем hardhat hetwork в новой сессии tmux в фоновом режиме
tmux new-session -d -s hardhat_network 'npx hardhat node'

# Компилируем и проводим миграцию контрактов
npx hardhat compile
npx hardhat run scripts/deploy.js --network localhost
npx hardhat run scripts/getDate.js --network localhost

# закрытие фоновой сессии
tmux kill-session -t hardhat_network

echo "Experiment is completed"
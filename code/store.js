import fs from 'fs'
import { NFTStorage, File } from 'nft.storage'
import dotenv from 'dotenv'

dotenv.config()

const endpoint = 'https://api.nft.storage'
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDQ0OGNmQkVCN0FDQjBCNGRBZmI0RTQ3ZTczQTFlZTYzQjdBNGNiZDQiLCJpc3MiOiJuZnQtc3RvcmFnZSIsImlhdCI6MTcxMjA3NDI1MzE1MSwibmFtZSI6Im5ld3N0b3JlIn0.EBVb5pfjV9GXvcnQHL3_kSy1_Yxl4mLr-vELM4sQGeE' // your API key from https://nft.storage/manage

async function main() {
  const storage = new NFTStorage({ endpoint, token })
  const metadata = await storage.store({
    name: 'Document PDF',
    description:
      'Using the nft.storage metadata API to create ERC-1155 compatible metadata.',
    image: new File([await fs.promises.readFile('nft.png')], 'nft.png', {
      type: 'image/jpg',
    }),
    properties: {
      custom:
        'Any custom data can appear in properties, files are automatically uploaded.',
      document: new File([await fs.promises.readFile('Document.pdf')], 'Document.pdf', {
        type: 'application/pdf',
      }),
    },
  })
  console.log('IPFS URL for the metadata:', metadata.url)
  console.log('metadata.json contents:\n', metadata.data)
  console.log(
    'metadata.json contents with IPFS gateway URLs:\n',
    metadata.embed()
  )
  const metadataContent = JSON.stringify(metadata.data, null, 2)
  fs.writeFileSync('metadata.json', metadataContent)

  console.log('metadata.json saved!')
  process.env.CID = metadata.url.split('/')[2];
  fs.writeFileSync('.env', `CID=${process.env.CID}`);
}
main()

const fs = require('fs');

async function main() {
  const transactionData = JSON.parse(fs.readFileSync('output.json'));
  const toAddress = transactionData.to;

  const TransactionContract = await ethers.getContractFactory("TransactionContract");
  const contract = TransactionContract.attach(toAddress);
  const trId = 0;
  await contract.confirmTransaction(trId);
  const trData = await contract.getAllTransactionData(trId);
  console.log(`Data transaction: 
  INITIALS_PERSON: ${trData[0]}
  ID_PERSON: ${trData[1]}
  DATE: ${trData[2]}
  CID: ${trData[3]}
  MENTIONS: ${trData[4]}
  Conform Transaction: ${trData[5]}
  `);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });

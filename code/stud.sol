// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

contract TransactionContract {
    struct Transaction {
        string initialsPerson;
        string idPerson;
        string date;
        string CID;
        string[] mentions;
        bool confirmed; // Add a confirmed field to the Transaction
    }

    mapping(uint256 => Transaction) transactions;
    uint256 public transactionCounter;

    mapping(address => bool) public teachers;

    constructor() {
        teachers[msg.sender] = true; // Deployer is initially set as a teacher
    }

    function addTeacher(address _teacher) external {
        require(teachers[msg.sender], "Only teachers can perform this action");
        teachers[_teacher] = true;
    }

    function createTransaction(
        string memory _initialsPerson,
        string memory _idPerson,
        string memory _date,
        string memory _CID,
        string[] memory _mentions
    ) external returns (uint256) {
        transactions[transactionCounter] = Transaction({
            initialsPerson: _initialsPerson,
            idPerson: _idPerson,
            date: _date,
            CID: _CID,
            mentions: _mentions,
            confirmed: false // Initialize confirmed as false
        });
        transactionCounter++;
        return transactionCounter - 1;
    }

    function confirmTransaction(uint256 _transactionId) external {
        require(teachers[msg.sender], "Only teachers can confirm transactions");
        require(!transactions[_transactionId].confirmed, "Transaction already confirmed");
        transactions[_transactionId].confirmed = true;
    }

    function getTransaction(
        uint256 _transactionId
    ) external view returns (Transaction memory) {
        return transactions[_transactionId];
    }

    function getAllTransactionData(uint256 _transactionId) external view returns (string memory, string memory, string memory, string memory, string[] memory, bool) {
        Transaction memory transaction = transactions[_transactionId];
        return (
            transaction.initialsPerson,
            transaction.idPerson,
            transaction.date,
            transaction.CID,
            transaction.mentions,
            transaction.confirmed
        );
    }
}

require('dotenv').config();
const fs = require('fs');

async function main() {
  const Contract = await ethers.getContractFactory("TransactionContract");
  const contract = await Contract.deploy();


  // Создаем транзакцию с данными из .env
  const transactionDate = await contract.createTransaction(
    process.env.INITIALS_PERSON,
    process.env.ID_PERSON,
    process.env.DATE,
    process.env.CID,
    [process.env.MENTIONS]
  );

  fs.writeFileSync('output.json', JSON.stringify(transactionDate, null, 2));
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
